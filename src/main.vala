using Gtk;

public class CosmosApp : Gtk.Application {
	construct {
		application_id = "com.cosmos.pannel";
	}

	public override void activate () {

		var win = new Gtk.ApplicationWindow (this);


		var monitors = Gdk.Display.get_default ().get_monitors ();
		var rectangle = ((Gdk.Monitor)monitors.get_item (0)).get_geometry ();
		win.set_default_size (rectangle.width, 48);

		var provider = new Gtk.CssProvider();
		provider.load_from_data(datastr.data);

		Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);


		GtkLayerShell.init_for_window (win);
		GtkLayerShell.set_layer (win, GtkLayerShell.Layer.TOP);
		GtkLayerShell.set_anchor (win, GtkLayerShell.Edge.BOTTOM, true);
		GtkLayerShell.auto_exclusive_zone_enable (win);

		var panel = new Panel();
		win.child = panel;

		panel.add_tile (new LaunchMenu());
		panel.add_tile(new Launcher("firefox", {"/usr/bin/firefox"}));
		panel.add_tile(new Launcher("chromium", {"chromium", "--enable-features=UseOzonePlatform", "--ozone-platform=wayland"}));
		panel.add_tile(new Launcher("org.gnome.Nautilus", {"/usr/bin/nautilus"}));
		panel.add_tile(new Launcher("deepin-terminal-gtk", {"/usr/bin/deepin-terminal-gtk"}));

		win.present ();
	}
}

int main (string[] args) {
	var app = new CosmosApp ();
	return app.run (args);
}

public class Panel : Gtk.Box {
	construct {

	}

	public Panel(){
		orientation = Gtk.Orientation.HORIZONTAL;
		spacing = 0;
	}

	public void add_tile(Tile tile){
		append(tile);
	}
}

public class Tile : Gtk.Box {
	construct {

	}
}

public class LaunchMenu : Tile {
	private Gtk.Button m_button = new Gtk.Button();
	construct {
		Gtk.Image m_image;
		if (FileUtils.test(Config.APPDIR + "/config/menu", FileTest.EXISTS))
			m_image = new Gtk.Image.from_file (Config.APPDIR + "/config/menu");
		else if (FileUtils.test(Environment.get_home_dir () + "/.config/cosmos-de/menu", FileTest.EXISTS))
			m_image = new Gtk.Image.from_file (Environment.get_home_dir () + "/.config/cosmos-de/menu");
		else
			m_image = new Gtk.Image.from_icon_name(name);
		m_image.set_pixel_size(32);
		m_button.child = m_image;
		m_button.has_frame = false;
		m_button.set_use_underline (true);
		m_button.clicked.connect(pop_menu);
		base.append(m_button);	
	}

	private void pop_menu(){
		Process.spawn_command_line_async("/usr/bin/cosmos-menu");
	}

}

// deprecated
public class Launcher: Tile {
	construct {

	}
	private string m_name;
	private string []m_args;
	private Gtk.Button m_button = new Gtk.Button();
	private Gtk.Image m_image;

	public Launcher(string name, string []args){
		m_name = name;
		m_args = args;
		print ("Creation de %s\n", m_name);

		m_image = new Gtk.Image.from_icon_name(name);
		m_image.set_pixel_size(32);

		m_button.child = m_image;
		m_button.has_frame = false;
		m_button.set_use_underline (true);
		m_button.clicked.connect(()=>{
			this.action();
		});
		base.append(m_button);
	}

	

	public void action () {
		print ("hello\n");
		print ("%s\n", m_name);
		print ("hello\n");
		// var s = string.joinv(string? separator, string?[]? str_array)
		// print(@"Executions de $(m_name) )\n");
		Process.spawn_async(null, m_args, null, GLib.SpawnFlags.STDOUT_TO_DEV_NULL | STDERR_TO_DEV_NULL, null, null);
	}
}



const string datastr = """
window {
	background-image:none;
	text-shadow:none;
	transition-timing-function:ease;
	-gtk-icon-shadow:none;
	border:none;
	outline:none;
	outline-offset:0px;
	padding: 0px;
	opacity:1;
}

window{
	background-color:rgba(80,80,80,0.5);
}
button{
	padding:5px;
}
button:hover{
	background-color:rgba(130,130,130,0.5);
	border-radius:13px;
}
""";
